let user =[];
let userSort=[];
const MIL = 1e6;

let millionaireToggle=false;
let sortToggle=false;
let totalWealthToggle=false;

const table = document.getElementById('personTable');
const addUserButton = document.getElementById('addUserBtn');
const showMillionaireButton = document.getElementById('showMillionaireBtn');
const sortButton = document.getElementById('sortBtn');
const totalWealthBtn = document.getElementById('totalWealthBtn');

function addUser(){
    totalWealthToggle=false;
    clearTotalWealth();
    getUser();
}

async function getUser() {
    try{
        addUserButton.disabled=true;
        const randomUserPromise = await fetch('https://randomuser.me/api/');
        const randomUserData = await randomUserPromise.json();
        const randomUser = randomUserData.results[0];
        const userWealth = Math.floor(Math.random()*MIL);
        let person = {
            name : randomUser.name.first+" "+randomUser.name.last,
            wealth : userWealth
        }
        user.push(person);
        addUserRow(person);
        addUserButton.disabled=false;
    }catch(err){
        console.log("Error in Fetching Random User");
        alert(err);
    }
    
}

function addUserRow(person){
    const row = table.insertRow(table.rows.length);
    const userCol = row.insertCell(0);
    const wealthCol = row.insertCell(1);
    userCol.innerText=person.name;
    wealthCol.innerText="$ "+person.wealth.toLocaleString("en-US");
}

function showUserDetails(){
    let userDetails = [];
    if(!sortToggle){
        userDetails=user;
    }else{
        userDetails=userSort;
    }
    let sum=0;
    table.innerHTML="";
    userDetails.map(person => {
        if(millionaireToggle){
            if(person.wealth>=MIL){
                sum+=parseInt(person.wealth);
                addUserRow(person);
            }
        }else{
            sum+=parseInt(person.wealth);
            addUserRow(person);
        }
    })
    if(totalWealthToggle){
        showTotalWealth(sum);
    }else{
        clearTotalWealth();
    }
}

function doubleMoney(){
    totalWealthToggle=false;
    user.forEach(person => {
        if(millionaireToggle){
            if(person.wealth>=MIL){
                person.wealth*=2;
            }
        }else{
            person.wealth*=2;
        }
        
    });
    showUserDetails();
}

function showMillionaires(){
    totalWealthToggle=false;
    if(!millionaireToggle){
        millionaireToggle=true;
        addUserButton.disabled=true;
        addUserButton.classList.add("btn3");
        showMillionaireButton.classList.toggle("btn2");
    }else{
        millionaireToggle=false;
        if(!sortToggle){
            addUserButton.disabled=false;
            addUserButton.classList.remove("btn3");
        }
        showMillionaireButton.classList.toggle("btn2");
    }
    showUserDetails();
}

function sortRichest(){
    totalWealthToggle=false;
    if(!sortToggle){
        sortToggle=true;
        addUserButton.disabled=true;
        addUserButton.classList.add("btn3");
        sortButton.classList.toggle("btn2");
        userSort = [...user].sort((firstUser, secondUser) => secondUser.wealth-firstUser.wealth);
    }else{
        sortToggle=false;
        if(!millionaireToggle){
            addUserButton.disabled=false;
            addUserButton.classList.remove("btn3");
        }
        sortButton.classList.toggle("btn2");
    }
    showUserDetails();
}

function calculateTotalWealth(){
    if(!totalWealthToggle){
        totalWealthToggle=true;
        showUserDetails();
        totalWealthBtn.classList.add("btn2");
    }else{
        totalWealthToggle=false;
        clearTotalWealth();
    }
}

function showTotalWealth(sum){
    const footer = table.createTFoot();
    const row = footer.insertRow(0);  
    const th1 = document.createElement("TH");
    const th2 = document.createElement("TH");
    th1.innerText = "Total Wealth";
    th2.innerText = "$ "+sum.toLocaleString("en-US");
    row.appendChild(th1); 
    row.appendChild(th2); 
}

function clearTotalWealth(){
    totalWealthBtn.classList.remove("btn2");
    table.deleteTFoot();
}